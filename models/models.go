// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    welcome, err := UnmarshalWelcome(bytes)
//    bytes, err = welcome.Marshal()

package models

import "encoding/json"

func UnmarshalWelcome(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Vacancy struct {
	ID                 int                `json:"id"`
	Context            string             `json:"@context"`
	Type               string             `json:"@type"`
	DatePosted         string             `json:"datePosted"`
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	Identifier         Identifier         `json:"identifier"`
	ValidThrough       string             `json:"validThrough"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	JobLocation        interface{}        `json:"jobLocation"`
	JobLocationType    string             `json:"jobLocationType"`
	EmploymentType     string             `json:"employmentType"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}

type Identifier struct {
	Type  string `json:"@type"`
	Name  string `json:"name"`
	Value string `json:"value"`
}

type JobLocation struct {
	Type    string  `json:"@type"`
	Address Address `json:"address"`
}

type Address struct {
	Type            string         `json:"@type"`
	StreetAddress   string         `json:"streetAddress"`
	AddressLocality string         `json:"addressLocality"`
	AddressCountry  AddressCountry `json:"addressCountry"`
}

type AddressCountry struct {
	Type string `json:"@type"`
	Name string `json:"name"`
}
