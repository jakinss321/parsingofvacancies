module main

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/swaggo/http-swagger/example/go-chi v0.0.0-20230327134356-bc837951e6c7
	github.com/swaggo/http-swagger/v2 v2.0.1
	github.com/swaggo/swag v1.16.1
	github.com/tebeka/selenium v0.9.9
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.9 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files/v2 v2.0.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/tools v0.9.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
