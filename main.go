package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"

	"main/models"
	"main/repository"
	"net/http"
	"regexp"
	"strconv"
	"time"

	_ "main/docs"

	httpSwagger "github.com/swaggo/http-swagger/v2"

	"github.com/PuerkitoBio/goquery"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

const maxTries = 5

// @title           Documentation of your project API.
// @version         1.0

// @contact.name   API CRUD app

// @host      localhost:8080

func main() {
	go newServer()

	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	defer wd.Quit()
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}

	defer wd.Quit()

	page := 1
	query := "golang"

	nextPage(page, query, wd)

	defer wd.Quit()

	vacCount, err := getVacCountRaw(wd)
	if err != nil {
		log.Fatal(err)
	}

	links, err := GetVacLinks(wd)
	if err != nil {
		log.Fatal(err)
	}

	numPage := vacCount / len(links)

	for numPage >= page {
		page++
		link, err := GetVacLinks(wd)
		if err != nil {
			log.Fatal(err)
		}
		links = append(links, link...)
		nextPage(page, query, wd)
		time.Sleep(time.Second)
	}

	for _, v := range links {
		ExampleScrape(v)
	}

}

func getVacCountRaw(wd selenium.WebDriver) (int, error) {
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		fmt.Println(err)
	}

	vacCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}
	re := regexp.MustCompile("[0-9]+")
	numStr := re.FindString(vacCountRaw)
	num, err := strconv.Atoi(numStr)
	if err != nil {
		return 0, err
	}
	return num, nil

}

func GetVacLinks(wd selenium.WebDriver) ([]string, error) {

	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return []string{}, err
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, link)

	}
	return links, nil
}

func nextPage(page int, query string, wd selenium.WebDriver) {
	wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
}

func ExampleScrape(link string) {
	var vac models.Vacancy
	preUrl := "https://career.habr.com"
	res, err := http.Get(preUrl + link)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		log.Println("notFound")
	}

	err = json.Unmarshal([]byte(dd.Text()), &vac)
	if err != nil {
		panic(err)
	}

	vac = vacController.storage.Create(vac)
	fmt.Println(vac)
}

var vacController = NewVacController()

func newServer() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Post("/vac", vacController.VacCreate)

	r.Get("/vac/{ID}", vacController.VacGetById)
	r.Get("/vacs", vacController.VacGetList)
	r.Delete("/vac/{ID}", vacController.VacDelete)
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

// @Summary CreateVacancy
// @Description  Создание вакансии.
// @Accept       json
// @Produce      json
// @Param        Vacancy body models.Vacancy true "Add vacancy"
// @Success      200  {object}  models.Vacancy
// @Router       /vac [post]
func (p *VacController) VacCreate(w http.ResponseWriter, r *http.Request) {
	var vac models.Vacancy
	err := json.NewDecoder(r.Body).Decode(&vac)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vac = p.storage.Create(vac)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vac)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// @Summary GetByIdVacancy
// @Description  Получаения вакансии по id.
// @Accept       json
// @Produce      json
// @Param        id   path      int  true  "Account ID"
// @Success      200  {object}  models.Vacancy
// @Router       /vac/{id} [get]
func (p *VacController) VacGetById(w http.ResponseWriter, r *http.Request) {
	var (
		vac   models.Vacancy
		err   error
		IDRaw string
		ID    int
	)

	IDRaw = chi.URLParam(r, "ID")

	ID, err = strconv.Atoi(IDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vac, err = p.storage.GetById(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vac)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// @Summary GetListVacancy
// @Description  Получаение списка вакансии.
// @Accept       json
// @Produce      json
// @Success      200  {object}  models.Vacancy
// @Router       /vacs [get]
func (p *VacController) VacGetList(w http.ResponseWriter, r *http.Request) {
	vacs := p.storage.GetList()

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(vacs)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// @Summary DeleteByIdVacancy
// @Description  Удаление вакансии по id.
// @Accept       json
// @Produce      json
// @Param        id   path      int  true  "Account ID"
// @Success      200  {object}  models.Vacancy
// @Router       /vac/{id} [delete]
func (p *VacController) VacDelete(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		IDRaw string
		ID    int
	)

	IDRaw = chi.URLParam(r, "ID")
	ID, err = strconv.Atoi(IDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = p.storage.Delete(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

type VacController struct {
	storage *repository.Vacancyg
}

func NewVacController() *VacController {
	return &VacController{storage: repository.NewVacancy()}
}

// go newServer()
// var caps = selenium.Capabilities{
// 	"browserName": "chrome",
// }
// var chromeCaps = chrome.Capabilities{
// 	Args: []string{
// 		"--headless",
// 	},
// }
// caps.AddChrome(chromeCaps)
// wd, err := selenium.NewRemote(caps, "http://localhost:9515")
// if err != nil {
// 	log.Fatal(err)
// }
