package repository

import (
	"fmt"
	"main/models"
	"sync"
)

type Vacansyger interface {
	Create(dto models.Vacancy) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type Vacancyg struct {
	data               []*models.Vacancy
	primaryKeyIDx      map[int]*models.Vacancy
	autoIncrementCount int
	sync.Mutex
}

func NewVacancy() *Vacancyg {
	return &Vacancyg{
		data:          make([]*models.Vacancy, 0, 13),
		primaryKeyIDx: make(map[int]*models.Vacancy, 13),
	}
}

func (p *Vacancyg) GetList() []models.Vacancy {
	var list []models.Vacancy
	for _, v := range p.data {
		list = append(list, *v)
	}
	return list
}

func (p *Vacancyg) GetById(petId int) (models.Vacancy, error) {
	if v, ok := p.primaryKeyIDx[int(petId)]; ok {
		return *v, nil
	}
	return models.Vacancy{}, fmt.Errorf("not found")
}

func (p *Vacancyg) Delete(Id int) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[Id]; !ok {
		return fmt.Errorf("not found")
	}
	for i, vac := range p.data {
		if vac.ID == Id {
			p.data = append(p.data[:i], p.data[i+1:]...)
			delete(p.primaryKeyIDx, Id)
			return nil
		}
	}
	return fmt.Errorf("not found")
}

func (p *Vacancyg) Create(pet models.Vacancy) models.Vacancy {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[int(pet.ID)] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)
	return pet
}
